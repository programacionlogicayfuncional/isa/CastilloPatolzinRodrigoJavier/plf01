(ns plf01.core)

(defn funcion-<-1
  [a]
  (< a))

(defn funcion-<-2
  [a b]
  (< a b))

(defn funcion-<-3
  [a b c]
  (< a b c))

(funcion-<-1 2)
(funcion-<-2 3 4)
(funcion-<-3 3 7 2)

(defn funcion-<=-1
  [a b]
  (<= a b))

(defn funcion-<=-2
  [a b c]
  (<= a b c))

(defn funcion-<=-3
  [a b c d]
  (<= a b c d))

(funcion-<=-1 2 3)
(funcion-<=-2 2 3 5)
(funcion-<=-3 2 2 5 7)

(defn funcion-==-1
  [a b]
  (== a b))

(defn funcion-==-2
  [a b c]
  (== a b c))

(defn funcion-==-3
  [a b c d]
  (== a b c d)

  (funcion-==-1 1.0 1)
  (funcion-==-2 1 1.0 2))
  (funcion-==-3 1/1 2/2 3/3 4/4)

(defn funcion->-1
  [a b]
  (> a b))

(defn funcion->-2
  [a b c]
  (> a b c))

(defn funcion->-3
  [a b c d]
  (> a b c d))

(funcion->-1 5 2)
(funcion->-2 3 2 4)
(funcion->-3 40/5 5 3 2)

(defn funcion->=-1
  [a b]
  (>= a b))

(defn funcion->=-2
  [a b c]
  (>= a b c))

(defn funcion->=-3
  [a b c d]
  (>= a b c d))

(funcion->=-1 1/2 0.5)
(funcion->=-2 1/2 1/3 1/4)
(funcion->=-3 10 7 4 2)

(defn funcion-assoc-1
  [posicion valor]
  (assoc [10 40 60 30] posicion valor))

(defn funcion-assoc-2
  [posicion valor]
  (assoc [10 40 60 30 3 8] posicion valor))

(defn funcion-assoc-3
  [posicion valor]
  (assoc [10 40 60 30 3 \a] posicion valor))

(funcion-assoc-1 0 2)
(funcion-assoc-2 3 \a)
(funcion-assoc-3 1 \s)

(defn funcion-assoc-in-1
  [a C D]
  (assoc-in {} [a] {C D}))

(defn funcion-assoc-in-2
  [a b  E F]
  (assoc-in {} [a b] {E F}))

(defn funcion-assoc-in-3
  [a b c  E F]
  (assoc-in {} [a b c] {E F}))

(funcion-assoc-in-1 {} [1] {2 3})
(funcion-assoc-in-2 {} [1 2] 2 2)
(funcion-assoc-in-3 2 [1 2 3] 2 2 2)

(defn funcion-concat-1
 [a b] 
  (concat a b))

(defn funcion-concat-2
  [a b c]
  (concat a b c))

(defn funcion-concat-3
  [a b A B]
  (concat a b A B))

(funcion-concat-1 [\a] [3])
(funcion-concat-2 [2 3] [4 \a] [1 2])
(funcion-concat-3 [2 3] [4 \a] [1 2] [5 9])

(defn funcion-conj-1
  [a B]
  (conj a B))

 (defn funcion-conj-2
   [a b C]
   (conj [a] b C))

(defn funcion-conj-3
  [a b c]
  (conj a b c))

(funcion-conj-1 [3 4] 2)
(funcion-conj-2 [3 4] [\a \b] 10)
(funcion-conj-3 [2] [5] [2 3])


(defn funcion-cons-1
  [A b]
  (cons A b))

(defn funcion-cons-2
  [a b]
  (cons a b))

 (defn funcion-cons-3
   [A b]
   (cons A b))

(funcion-cons-1 1 [2 3 4])
(funcion-cons-2 [1 2 3] [4 5 6])
(funcion-cons-3 1 [\a \b \c])

(defn funcion-contains?-1
  [a B]
  (contains? a B))

(defn funcion-contains?-2
  [a b  A]
  (contains? {a b} A))

(defn funcion-contains?-3
  [a b c  D]
  (contains? #{a b c} D))

(funcion-contains?-1 [1 2 3 4 5] 3)
(funcion-contains?-2 \a \b  \a)
(funcion-contains?-3 :a :b :C  :a)


(defn funcion-count-1
  [a]
  (count a))

(defn funcion-count-2
  [a b]
  (count [a b]))

(defn funcion-count-3
  [a b c d]
  (count {a b c d}))

(funcion-count-1 [2 3 4 5 6 7 8])
(funcion-count-2 [\a \b] [\b \c])
(funcion-count-3 :a :b :c :d)


(defn funcion-disj-1
  [a B]
  (disj a B))

(defn funcion-disj-2
  [a B]
  (disj a B))

(defn funcion-disj-3
  [a B C]
  (disj a B C))

(funcion-disj-1 #{1 2 3} 2)
(funcion-disj-1 #{1 2 3} 6)
(funcion-disj-3 #{1 2 3} 1 3)

